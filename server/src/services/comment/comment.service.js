class Comment {
  constructor({ commentRepository }) {
    this._commentRepository = commentRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  update(userId, commentId, body) {
    if (!this.doesCommentBelongToUser(userId, commentId)) {
      throw Error('User can only update comments that belong to him.');
    }
    return this._commentRepository.updateById(commentId, body);
  }

  delete(userId, commentId) {
    if (!this.doesCommentBelongToUser(userId, commentId)) {
      throw Error('User can only delete comments that belong to him.');
    }
    return this._commentRepository.deleteById(commentId);
  }

  async doesCommentBelongToUser(userId, commentId) {
    const comment = await this._commentRepository.getCommentById(commentId);
    return userId === comment?.user.id;
  }
}

export { Comment };
