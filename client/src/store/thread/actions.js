import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  EDIT_POST: 'thread/edit-post',
  DELETE_POST: 'thread/delete-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const editPost = createAction(ActionType.EDIT_POST, post => ({
  payload: {
    post
  }
}));

const deletePost = createAction(ActionType.DELETE_POST, id => ({
  payload: {
    id
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost = (id, post) => async dispatch => {
  const { id: updatedPostId } = await postService.updatePost(id, post);
  const updatedPost = await postService.getPost(updatedPostId);
  dispatch(editPost(updatedPost));
};

const removePost = id => async dispatch => {
  await postService.deletePost(id);
  dispatch(deletePost(id));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const setPostReaction = (postId, isLike) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.setPostReaction(
    postId,
    isLike
  );
  const isUpdate = Boolean(new Date(updatedAt) - new Date(createdAt));

  // TODO maybe better implementation of removing previous reaction
  const oppositeDiff = isUpdate ? -1 : 0;
  const diff = id ? 1 : -1;

  const mapReaction = isLike
    ? post => ({
      ...post,
      dislikeCount: (Number(post.dislikeCount) + oppositeDiff).toString(),
      likeCount: (Number(post.likeCount) + diff).toString() // diff is taken from the current closure
    })
    : post => ({
      ...post,
      likeCount: (Number(post.likeCount) + oppositeDiff).toString(),
      dislikeCount: (Number(post.dislikeCount) + diff).toString()
    });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapReaction(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const editComment = (commentId, body) => async (dispatch, getRootState) => {
  const { id } = await commentService.editComment(commentId, body);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    comments: post.comments?.map(c => (c.id === id ? comment : c))
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = comment => async (dispatch, getRootState) => {
  await commentService.deleteComment(comment.id);

  const mapComments = post => ({
    ...post,
    comments: post.comments?.filter(c => c.id !== comment.id)
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  editPost,
  deletePost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  removePost,
  toggleExpandedPost,
  setPostReaction,
  addComment,
  editComment,
  deleteComment
};
