import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  editComment(commentId, body) {
    return this._http.load(`/api/comments/${commentId}`, {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(body)
    });
  }

  deleteComment(commentId) {
    return this._http.send(`/api/comments/${commentId}`, {
      method: HttpMethod.DELETE
    });
  }
}

export { Comment };
