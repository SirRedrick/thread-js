import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost, EditPost, DeletePost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [editedPost, setEditedPost] = React.useState(undefined);
  const [toBeDeletedPostId, setToBeDeletedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(
    id => dispatch(threadActionCreator.setPostReaction(id, true)),
    [dispatch]
  );

  const handlePostDislike = React.useCallback(
    id => dispatch(threadActionCreator.setPostReaction(id, false)),
    [dispatch]
  );

  const handleExpandedPostToggle = React.useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = React.useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostEdit = React.useCallback(
    (id, postPayload) => dispatch(threadActionCreator.updatePost(id, postPayload)),
    [dispatch]
  );

  const handlePostDeletion = React.useCallback(
    id => dispatch(threadActionCreator.removePost(id)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const editPost = id => setEditedPost(posts.find(post => post.id === id));

  const deletePost = id => setToBeDeletedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => {
          const postBelongsToUser = post.userId === userId;

          return (
            <Post
              post={post}
              onPostLike={handlePostLike}
              onPostDislike={handlePostDislike}
              onExpandedPostToggle={handleExpandedPostToggle}
              sharePost={sharePost}
              editPost={postBelongsToUser ? editPost : null}
              deletePost={postBelongsToUser ? deletePost : null}
              key={post.id}
            />
          );
        })}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost userId={userId} sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
      {editedPost && (
        <EditPost
          editedPost={editedPost}
          onPostEdit={handlePostEdit}
          uploadImage={uploadImage}
          close={() => setEditedPost(undefined)}
        />
      )}
      {toBeDeletedPostId && (
        <DeletePost
          toBeDeletedPostId={toBeDeletedPostId}
          handlePostDeletion={handlePostDeletion}
          close={() => setToBeDeletedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
