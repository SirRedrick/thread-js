import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'src/components/common/common';
import { ButtonColor, ButtonType } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const DeletePost = ({ toBeDeletedPostId, handlePostDeletion, close }) => {
  const onSubmit = async () => {
    await handlePostDeletion(toBeDeletedPostId);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header>
        <span>Are you sure you want to delete this post?</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={onSubmit}>
          <div className={styles.btnWrapper}>
            <Button onClick={close} color={ButtonColor.BLUE} type={ButtonType.BUTTON}>
              Cancel
            </Button>
            <Button color={ButtonColor.RED} type={ButtonType.SUBMIT}>
              Delete
            </Button>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

DeletePost.propTypes = {
  toBeDeletedPostId: PropTypes.string.isRequired,
  handlePostDeletion: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default DeletePost;

