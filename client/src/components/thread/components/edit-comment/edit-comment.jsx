import * as React from 'react';
import PropTypes from 'prop-types';
import { Form, Comment as CommentUI } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { commentType } from 'src/common/prop-types/prop-types';
import { ButtonType, ButtonColor, ButtonSize } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const EditComment = ({ comment, handleCommentEdit, handleCancelEditing }) => {
  const [body, setBody] = React.useState(comment.body);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={comment.user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <Form onSubmit={() => handleCommentEdit(comment.id, { body })}>
          <Form.TextArea
            className={styles.textarea}
            name="body"
            value={body}
            placeholder="What is different?"
            onChange={ev => setBody(ev.target.value)}
          />
          <div className={styles.buttons}>
            <Form.Button
              color={ButtonColor.BLUE}
              type={ButtonType.SUBMIT}
              size={ButtonSize.MINI}
            >
              Confirm
            </Form.Button>
            <Form.Button
              color={ButtonColor.RED}
              type={ButtonType.BUTTON}
              size={ButtonSize.MINI}
              onClick={handleCancelEditing}
            >
              Cancel
            </Form.Button>
          </div>
        </Form>
      </CommentUI.Content>
    </CommentUI>
  );
};

EditComment.propTypes = {
  comment: commentType.isRequired,
  handleCommentEdit: PropTypes.func.isRequired,
  handleCancelEditing: PropTypes.func.isRequired
};

export default EditComment;
