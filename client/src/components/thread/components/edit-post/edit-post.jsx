import * as React from 'react';
import PropTypes from 'prop-types';
import { postType } from 'src/common/prop-types/prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Modal, Form, Image, Button } from 'src/components/common/common';

import styles from './styles.module.scss';

const EditPost = ({ editedPost, onPostEdit, uploadImage, close }) => {
  const [body, setBody] = React.useState(editedPost.body);
  const [image, setImage] = React.useState(editedPost.image);
  const [isUploading, setIsUploading] = React.useState(false);

  const handlePostEdit = async () => {
    if (!body) {
      return;
    }
    await onPostEdit(editedPost.id, { imageId: image?.imageId, body });
    close();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => setIsUploading(false));
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handlePostEdit}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What is different?"
            onChange={ev => setBody(ev.target.value)}
          />
          {image?.imageLink && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.imageLink} alt="post" />
            </div>
          )}
          <div className={styles.btnWrapper}>
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
            <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
              Post
            </Button>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

EditPost.propTypes = {
  editedPost: postType.isRequired,
  onPostEdit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditPost;
