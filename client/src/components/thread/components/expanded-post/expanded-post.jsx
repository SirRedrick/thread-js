import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import EditComment from '../edit-comment/edit-comment';
import DeleteComment from '../delete-comment/delete-comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  userId,
  sharePost
}) => {
  const [editedComment, setEditedComment] = React.useState(undefined);
  const [commentToBeDeleted, setCommentToBeDeleted] = React.useState(undefined);

  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleCommentEdit = React.useCallback((commentId, body) => (
    dispatch(threadActionCreator.editComment(commentId, body))
  ), [dispatch]);

  const handleCommentDeletion = React.useCallback(comment => (
    dispatch(threadActionCreator.deleteComment(comment))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const onCommentEdit = async (commentId, body) => {
    await handleCommentEdit(commentId, body);
    setEditedComment(undefined);
  };

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (editedComment === comment.id
              ? (
                <EditComment
                  key={comment.id}
                  comment={comment}
                  handleCommentEdit={onCommentEdit}
                  handleCancelEditing={() => setEditedComment(undefined)}
                />
              )
              : (
                <Comment
                  key={comment.id}
                  comment={comment}
                  belongsToCurrentUser={comment.userId === userId}
                  onCommentEdit={() => setEditedComment(comment.id)}
                  onCommentDelete={() => setCommentToBeDeleted(comment)}
                />
              )))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>

      ) : (
        <Spinner />
      )}
      {commentToBeDeleted && (
        <DeleteComment
          toBeDeletedComment={commentToBeDeleted}
          handleCommentDeletion={handleCommentDeletion}
          close={() => setCommentToBeDeleted(undefined)}
        />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  userId: PropTypes.string.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
