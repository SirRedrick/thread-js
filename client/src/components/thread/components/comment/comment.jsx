import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Icon, Comment as CommentUI } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import { IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user },
  belongsToCurrentUser,
  onCommentEdit,
  onCommentDelete
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
      <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
      <CommentUI.Text>{body}</CommentUI.Text>
    </CommentUI.Content>
    {belongsToCurrentUser && (
      <CommentUI.Actions>
        <CommentUI.Action onClick={onCommentEdit}>
          <Icon name={IconName.EDIT} />
        </CommentUI.Action>
        <CommentUI.Action onClick={onCommentDelete}>
          <Icon name={IconName.DELETE} />
        </CommentUI.Action>
      </CommentUI.Actions>
    )}
  </CommentUI>
);

Comment.propTypes = {
  comment: commentType.isRequired,
  belongsToCurrentUser: PropTypes.bool.isRequired,
  onCommentEdit: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;
