import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'src/components/common/common';
import { ButtonColor, ButtonType } from 'src/common/enums/enums';
import { commentType } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

const DeleteComment = ({ toBeDeletedComment, handleCommentDeletion, close }) => {
  const onSubmit = async () => {
    await handleCommentDeletion(toBeDeletedComment);
    close();
  };

  return (
    <Modal
      open
      size="small"
    >
      <Modal.Header>
        <span>Are you sure you want do delete this comment?</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={onSubmit}>
          <div className={styles.btnWrapper}>
            <Button onClick={close} color={ButtonColor.BLUE} type={ButtonType.BUTTON}>
              Cancel
            </Button>
            <Button color={ButtonColor.RED} type={ButtonType.SUBMIT}>
              Delete
            </Button>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

DeleteComment.propTypes = {
  toBeDeletedComment: commentType.isRequired,
  handleCommentDeletion: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default DeleteComment;
