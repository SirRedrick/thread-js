const ButtonSize = {
  LARGE: 'large',
  MINI: 'mini'
};

export { ButtonSize };
